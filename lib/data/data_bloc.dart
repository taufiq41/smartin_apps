import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:smartin/model/data_dumy.dart' as prefix0;
import 'data.dart';
import 'package:smartin/repository/data_repository.dart';
import 'package:smartin/model/data_dumy.dart';

class DataBloc extends Bloc<DataEvent, DataState> {
  DataRepository dataRepository;

  DataBloc({this.dataRepository});
  
  DataState get initialState => new Loading();

  @override
  Stream<DataState> mapEventToState(
    DataEvent event,
  ) async* {
    
    if(event is Fetch){
      try {
        List items = await dataRepository.getData();
        List<ListItem> listItem = List();
          
          for(var row in items){
            Map<String, dynamic> json = row;
            DataDummy dataDummy =  DataDummy.fromJson(json);
            ListItem listItem2 = ListItem(dataDummy: dataDummy, id: dataDummy.id, isDeleting: false);
            listItem.add(listItem2);
          }

        yield Loaded(listItem: listItem);

      } catch (_) {
        yield Failure();
      }
    }

    if(event is Delete){

    }
  }
}
