import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:smartin/model/data_dumy.dart';

class ListItem extends Equatable {
  final int id;
  DataDummy dataDummy;
  final bool isDeleting;

  ListItem({
    @required this.id,
    @required this.dataDummy,
    this.isDeleting = false,
  }) : super([id, dataDummy, isDeleting]);

  ListItem copyWith({
    int id,
    DataDummy dataDummy,
    bool isDeleting,
  }) {
    return ListItem(
      id: id ?? this.id,
      dataDummy: dataDummy ?? this.dataDummy,
      isDeleting: isDeleting ?? this.isDeleting,
    );
  }

  @override
  String toString() =>
      'ListItem { id: $id, value: $dataDummy, isDeleting: $isDeleting }';
}
