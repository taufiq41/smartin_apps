import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:smartin/data/data_item_state.dart';

@immutable
abstract class DataState extends Equatable {
  DataState([List props = const []]) : super(props);
}

class Loading extends DataState{
  @override
  String toString() => 'Loading';
}

class Loaded extends DataState{

  final List<ListItem> listItem;
  Loaded({this.listItem}) : super([listItem]);  
  
  @override
  String toString() => 'Loaded data';

}

class Failure extends DataState{

  @override
  String toString() => 'Failure';

}

