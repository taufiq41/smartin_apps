import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class DataEvent extends Equatable {
  DataEvent([List props = const []]) : super(props);
}

class Fetch extends DataEvent {
  @override
  String toString() => 'Fetch';
}

class Delete extends DataEvent {
  final String id;

  Delete({@required this.id}) : super([id]);

  @override
  String toString() => 'Delete { id: $id }';
}

class Deleted extends DataEvent {
  final String id;

  Deleted({@required this.id}) : super([id]);

  @override
  String toString() => 'Deleted { id: $id }';
}
