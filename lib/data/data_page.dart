import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'data.dart';
import 'package:smartin/repository/data_repository.dart';

class DataPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Data"),
      ),
      body: BlocProvider(
        builder: (context) => DataBloc(dataRepository: DataRepository())..dispatch(Fetch()),
        child: DataScreen(),
      )
    );
  }
}

class DataScreen extends StatefulWidget {
  
 /*  DataBloc dataBloc;
  DataScreen({this.dataBloc}); */
  
  @override
  _DataScreenState createState() => _DataScreenState();
}

class _DataScreenState extends State<DataScreen> {

  @override
  Widget build(BuildContext context) {

      final dataBloc = BlocProvider.of<DataBloc>(context);
      return BlocBuilder<DataBloc, DataState>(
        builder: (context, state) {
        
          if(state is Loaded){

            if(state.listItem.isEmpty){
              return Center(
                child: Text('no content'),
              );
            }
            
            return ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                return ItemTile(
                  item: state.listItem[index],
                  onDeletePressed: (id) {
                    dataBloc.dispatch(Delete(id: id));
                  },
                );
              },
              itemCount: state.listItem.length,
            );

          }

          if(state is Failure){
            return Center(
              child: Text('Oops something went wrong!'),
            );
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        }
      );

  }
}

class ItemTile extends StatelessWidget {
  final ListItem item;
  final Function(String) onDeletePressed;

  const ItemTile({
    Key key,
    @required this.item,
    @required this.onDeletePressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Text('#${item.id}'),
      title: Text(item.dataDummy.title),
      trailing: item.isDeleting
          ? CircularProgressIndicator()
          : IconButton(
              icon: Icon(Icons.delete, color: Colors.red),
              onPressed: () => onDeletePressed(item.id.toString()),
            ),
    );
  }
}
