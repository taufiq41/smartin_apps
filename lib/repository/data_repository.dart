import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

class DataRepository{

  String URL = 'https://jsonplaceholder.typicode.com/todos';

  Future<List> getData() async{
    try {
      final response = await http.get(URL);
      /* final response = await http.get(URL, headers: {'Accept' : 'application/json'}); */
      if (response.statusCode == 200) {
          
          List items = json.decode(response.body);
          
          return items;
          
      } else {
        print('error response');
        return null;
      }
    } catch (ex) {
      print('kesalahan 2'+ex.toString());
      return null;
    }
  }

}