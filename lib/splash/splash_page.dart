import 'package:flutter/material.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.green[300],
      child: Center(
        child : Text('Welcome in Smartin APPS',
          style: TextStyle(fontSize: 30, color: Colors.white),
        ),
      )
    );
  }
}