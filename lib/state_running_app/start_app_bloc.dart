import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:smartin/home/home_event.dart';
import 'start_app_event.dart';
import 'start_app_state.dart';


class StartAppBloc extends Bloc<StartAppEvent, StartAppState> {
  static final StartAppBloc _startAppBlocSingleton = new StartAppBloc._internal();
  factory StartAppBloc() {
    return _startAppBlocSingleton;
  }
  StartAppBloc._internal();
  
  StartAppState get initialState => new UnStartAppState();

  @override
  Stream<StartAppState> mapEventToState(
    StartAppEvent event,
  ) async* {
    if(event is LoadStartAppEvent){
      yield InStartAppState();
    }
  }
}
