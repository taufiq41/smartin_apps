import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class StartAppState extends Equatable {
  final Iterable propss;
  StartAppState([this.propss]);

  /// Copy object for use in action
  StartAppState getStateCopy();

  @override
  List<Object> get props => [propss];
}

/// UnInitialized
class UnStartAppState extends StartAppState {
  @override
  String toString() => 'UnStartAppState';

  @override
  StartAppState getStateCopy() {
    return UnStartAppState();
  }
}

/// Initialized
class InStartAppState extends StartAppState {
  @override
  String toString() => 'InStartAppState';

  @override
  StartAppState getStateCopy() {
    return InStartAppState();
  }
}

class ErrorStartAppState extends StartAppState {
  final String errorMessage;

  ErrorStartAppState(this.errorMessage);
  
  @override
  String toString() => 'ErrorStartAppState';

  @override
  StartAppState getStateCopy() {
    return ErrorStartAppState(this.errorMessage);
  }
}
