import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
abstract class StartAppEvent extends Equatable{
   StartAppEvent([List props = const []]) : super(props);
}

class LoadStartAppEvent extends StartAppEvent {
  @override
  String toString() => 'AppStarted';
}

class LoadMainMenuEvent extends StartAppEvent {
  @override
  String toString() => 'Load Main Menu';
}
