
class DataDummy{

  int userId;
  int id;
  String title;
  bool completed;

  DataDummy({this.userId, this.id, this.title, this.completed});

  factory DataDummy.fromJson(Map<String, dynamic> json) {
    return DataDummy(
      userId: json['userId'],
      id: json['id'],
      title: json['title'],
      completed: json['completed']
    );
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'userId': userId,
      'id': id,
      'title' : title,
      'completed' : completed
    };
    return map;
  }
}