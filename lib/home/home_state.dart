import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
class FormRigstrasiState extends Equatable {

  final String email, nama, noHp, posisiJabatan, gajiDiharapkan;
  final bool isEmail, isNama, isNoHp, isGajiDiharapkan, isPosisiJabatan, isSubmitSuccessfully;

  FormRigstrasiState({
    @required this.email, 
    @required this.nama, 
    @required this.posisiJabatan, 
    @required this.gajiDiharapkan,
    @required this.noHp, 
    @required this.isEmail, 
    @required this.isNama, 
    @required this.isNoHp, 
    @required this.isGajiDiharapkan, 
    @required this.isPosisiJabatan,
    @required this.isSubmitSuccessfully}
  ) : super([
          email,
          isEmail,
          nama,
          isNama,
          noHp,
          isNoHp,
          posisiJabatan,
          isPosisiJabatan,
          gajiDiharapkan,
          isGajiDiharapkan,
          isSubmitSuccessfully,
        ]);

  bool get isFormValid => isGajiDiharapkan && isEmail && isNama && isNoHp && isPosisiJabatan;

  factory FormRigstrasiState.initial(){
    return FormRigstrasiState(email: '', nama: '', posisiJabatan: null, noHp: '', gajiDiharapkan: '', isEmail: false, isNama: false, isNoHp: false, isGajiDiharapkan: false, isPosisiJabatan: false, isSubmitSuccessfully: false);
  }

  FormRigstrasiState copyWith({
    String email, String nama, String noHp, String posisiJabatan, String gajiDiharapkan,
    bool isEmail, bool isNama, bool isNoHp, bool isFormValid, bool isPosisiJabatan, bool isGajiDiharapkan, bool isSubmitSuccessFully 
  }){
    /* print('email '+(email ?? this.email));
    print('is email '+(isEmail ?? this.isEmail).toString());
    print(nama ?? this.nama);
    print(isNama ?? this.isNama);
    print(posisiJabatan ?? this.posisiJabatan);
    print(isPosisiJabatan ?? this.isPosisiJabatan);
    print(gajiDiharapkan ?? this.gajiDiharapkan);
    print(isGajiDiharapkan ?? this.isGajiDiharapkan);
    print(noHp ?? this.noHp);
    print(isNoHp ?? this.isNoHp);
    print(isSubmitSuccessfully ?? this.isSubmitSuccessfully); */
    
    return FormRigstrasiState(
      email: email ?? this.email,
      isEmail: isEmail ?? this.isEmail,
      nama: nama ?? this.nama,
      isNama : isNama ?? this.isNama,
      posisiJabatan: posisiJabatan ?? this.posisiJabatan,
      isPosisiJabatan: isPosisiJabatan ?? this.isPosisiJabatan,
      gajiDiharapkan: gajiDiharapkan ?? this.gajiDiharapkan,
      isGajiDiharapkan: isGajiDiharapkan ?? this.isGajiDiharapkan,
      noHp: noHp ?? this.noHp,
      isNoHp: isNoHp ?? this.isNoHp,
      isSubmitSuccessfully: isSubmitSuccessFully ?? this.isSubmitSuccessfully,
      );
  }

  @override
  String toString() {
    return '''FormRigstrasiState {
      email: $email,
      isEmailValid: $isEmail,
      nama: $nama,
      isNama: $isNama,
      isSubmitSuccessfully: $isSubmitSuccessfully
    }''';
  }

}
