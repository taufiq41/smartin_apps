import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'home.dart';
import 'package:smartin/painter/default_painter.dart';

class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      body: Container(
        child: BlocProvider(
          builder: (context) => HomeBloc(),
          child: HomeScreen(),
        ),
      ),
    );
  }
}


class HomeScreen extends StatefulWidget {

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  
  final _formKey = GlobalKey<FormState>();
  HomeBloc _homeBloc;
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _namaController = TextEditingController();
  final TextEditingController _gajiDiharapkanController = TextEditingController();
  final TextEditingController _noHpController = TextEditingController();
  String _posisiJabatan, email, nama, noHp, gajiDiharapkan;
  
  @override
  void initState() {
    super.initState();
    _homeBloc = BlocProvider.of<HomeBloc>(context);
    _emailController.addListener(_onEmailChanged);
    _namaController.addListener(_onNamaChanged);
    _noHpController.addListener(_noHPChanged);
    _gajiDiharapkanController.addListener(_onGajiDiharapkan);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, FormRigstrasiState>(      
      builder: (context, state) {
        print(state);
        if(state.isSubmitSuccessfully){
          return SuccessDialog(onDismissed: () {
             
          });
        }
        
        return Container(
          child: CustomPaint(
            painter: DefaultPainter(),
            child: SafeArea(

              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Card(
                      color: Colors.white,
                      
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            children: <Widget>[
                              TextFormField(
                                controller : _namaController,
                                decoration: InputDecoration(
                                  contentPadding: new EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                  enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                ),
                                autovalidate: true,
                                validator: (_){
                                  return state.isNama ? null : 'Nama harus di isi';
                                },
                                onSaved: (input) => nama = input,
                              ),
                              SizedBox(height : 20),
                              TextFormField(
                                controller : _emailController,
                                decoration: InputDecoration(
                                  contentPadding: new EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                  enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                ),
                                autovalidate: true,
                                validator: (input){
                                  
                                  return state.isEmail ? null : 'Email tidak valid';
                                },
                                onSaved: (input) => email = input,
                              ),
                              SizedBox(height : 20),
                              TextFormField(
                                controller: _noHpController,
                                decoration: InputDecoration(
                                  contentPadding: new EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                  enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                ),
                                autovalidate: true,
                                validator: (input){
                                  return state.isNoHp ? null : 'No HP tidak valid';
                                },
                                onSaved: (input) => noHp = input,
                                keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false)
                              ),
                              SizedBox(height : 20),
                              DropdownButtonFormField<String>(
                                decoration: InputDecoration(
                                  contentPadding: new EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                  enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                  fillColor: Colors.red,
                                ),
                                items: [
                                  DropdownMenuItem(
                                    value: '1',
                                    child: Text('Full Stack Web Programmer', style: TextStyle(fontSize: 18, color: Colors.blueAccent),),
                                  ),
                                  DropdownMenuItem(
                                    value: '2',
                                    child: Text('Full Stack Mobile Programmer', style: TextStyle(fontSize: 18, color: Colors.blueAccent),),
                                  ),
                                ],
                                value: state.posisiJabatan,
                                onChanged: (value) => _onPosisiJabatan(value),
                                hint: Text('Pilih Posisi Pekerjaan'),
                                validator: (value) => value == null ? 'Pilih dulu jabatan yang di inginkan' : null,
                                onSaved: (value) => _posisiJabatan = value,
                              ),
                              SizedBox(height: 20),
                              TextFormField(
                                controller: _gajiDiharapkanController,
                                decoration: InputDecoration(
                                  contentPadding: new EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                  enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                ),
                                autovalidate: true,
                                validator: (input){
                                  return state.isGajiDiharapkan ? null : 'Gaji yang di inginkan belum di isi';
                                },
                                onSaved: (input) => gajiDiharapkan = input,
                                keyboardType: TextInputType.number,
                              ),
                              SizedBox(height: 20,),
                              RaisedButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Text('Submit', style: TextStyle(fontSize: 15, color: Colors.white, fontWeight: FontWeight.bold),),
                                onPressed: () => state.isFormValid ? _submit() : {},
                                color: Colors.blue,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ]
                ),
              ),
            )
          ),
        );
      }
    );
  }

  _submit(){
    _homeBloc.dispatch(FormSubmitEvent());
  }

  @override
  void dispose() {
    _emailController.dispose();
    _namaController.dispose();
    _noHpController.dispose();
    _gajiDiharapkanController.dispose();
    super.dispose();
  }

  void _onEmailChanged() {
    _homeBloc.dispatch(EmailChangedEvent(email : _emailController.text));
  }

  void _onNamaChanged() {
    _homeBloc.dispatch(NamaChangedEvent(nama : _namaController.text));
  }

  void _noHPChanged() {
    _homeBloc.dispatch(NoHpChangedEvent(noHp : _noHpController.text));
  }

  void _onGajiDiharapkan(){
    _homeBloc.dispatch(GajiDiharapkanChangedEvent(gajiDiharapkan: _gajiDiharapkanController.text));
  }

  void _onPosisiJabatan(String posisiJabatan){
    _homeBloc.dispatch(PosisiJabatanChangedEvent(posisiJabatan: posisiJabatan));
  }

  void _onSubmitPressed(){
    _homeBloc.dispatch(FormSubmitEvent());
  }
  
}
class SuccessDialog extends StatelessWidget {
  final VoidCallback onDismissed;

  SuccessDialog({Key key, @required this.onDismissed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Icon(Icons.info),
              Flexible(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Text(
                    'Form Submitted Successfully!',
                    softWrap: true,
                  ),
                ),
              ),
            ],
          ),
          RaisedButton(
            child: Text('OK'),
            onPressed: onDismissed,
          ),
        ],
      ),
    );
  }
}
