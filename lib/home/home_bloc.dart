import 'dart:async';
import 'package:bloc/bloc.dart';
import 'home.dart';

class HomeBloc extends Bloc<HomeEvent, FormRigstrasiState> {
  
  final RegExp _emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );

  @override
  FormRigstrasiState get initialState => FormRigstrasiState.initial();
  
  @override
  void onTransition(Transition<HomeEvent, FormRigstrasiState> transition) {
    print(transition);
  }

  @override
  Stream<FormRigstrasiState> mapEventToState(
    HomeEvent event,
  ) async* {
    if(event is EmailChangedEvent){
      yield state.copyWith(
        email: event.email,
        isEmail: _isEmailValid(event.email),
      );

    }
    if(event is NoHpChangedEvent){

      yield state.copyWith(
        noHp: event.noHp,
        isNoHp: _isNoHpValid(event.noHp),
      );

    }
    if(event is NamaChangedEvent){
      /* print('nama '+event.nama);
      print('valid nama'+_isNamaValid(event.nama).toString()); */
      yield state.copyWith(
        nama: event.nama,
        isNama: _isNamaValid(event.nama),
      );

    }
    if(event is GajiDiharapkanChangedEvent){

      yield state.copyWith(
        gajiDiharapkan: event.gajiDiharapkan,
        isGajiDiharapkan: _isGajiDiharapkan(event.gajiDiharapkan),
      );

    }
    if(event is PosisiJabatanChangedEvent){
      yield state.copyWith(
        posisiJabatan: event.posisiJabatan,
        isPosisiJabatan: _isPosisiJabatan(),
      );
    }
    if(event is FormSubmitEvent){
      
      yield state.copyWith(
        isSubmitSuccessFully: true,
      );
    }
  }

  bool _isEmailValid(String email) {
    return _emailRegExp.hasMatch(email);
  }
  
  bool _isNamaValid(String nama){
    return nama.length > 0;
  }

  bool _isGajiDiharapkan(String gajiDiharapkan){
    return (gajiDiharapkan.length > 0 && gajiDiharapkan != '0');
  }

  bool _isNoHpValid(String noHp){
    return (noHp.length >= 10 && noHp.length <= 13);
  }
  
  bool _isPosisiJabatan(){
    return true;
  }
}
