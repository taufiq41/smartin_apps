import 'dart:async';
import 'package:equatable/equatable.dart';
import 'home.dart';
import 'package:meta/meta.dart';

@immutable
abstract class HomeEvent extends Equatable{
  HomeEvent([List props = const []]) : super(props);
}

class LoadHomeEvent extends HomeEvent {
  @override
  String toString() => 'LoadHomeEvent';
}

class EmailChangedEvent extends HomeEvent{
  final String email;
  EmailChangedEvent({@required this.email}) : super([email]);
}

class NamaChangedEvent extends HomeEvent{
  final String nama;
  NamaChangedEvent({@required this.nama}) : super([nama]);
}

class NoHpChangedEvent extends HomeEvent{
  final String noHp;
  NoHpChangedEvent({@required this.noHp}) : super([noHp]);
}

class PosisiJabatanChangedEvent extends HomeEvent{
  final String posisiJabatan;
  PosisiJabatanChangedEvent({@required this.posisiJabatan}) : super([posisiJabatan]);
}

class GajiDiharapkanChangedEvent extends HomeEvent{
  final String gajiDiharapkan;
  GajiDiharapkanChangedEvent({@required this.gajiDiharapkan}) : super([gajiDiharapkan]);
}

class FormSubmitEvent extends HomeEvent{
  @override
  String toString() => 'Form submit';
}