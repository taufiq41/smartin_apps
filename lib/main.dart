import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'state_running_app/start_app.dart';
import 'splash/splash.dart';
import 'home/home.dart';
import 'package:flutter/services.dart';

class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    print(event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }

  @override
  void onError(Bloc bloc, Object error, StackTrace stacktrace) {
    super.onError(bloc, error, stacktrace);
    print(error);
  }
}

void main() {
  SystemChrome.setEnabledSystemUIOverlays([]);
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(
    BlocProvider<StartAppBloc>(
      builder: (context) {
        return StartAppBloc()..dispatch(LoadStartAppEvent());
      },
      child: App(),
    ),
  );
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BlocBuilder<StartAppBloc, StartAppState>(
        builder: (context, state) {
          
          if (state is InStartAppState) {
            return HomePage();
          }
          return SplashPage();
                    
        },
      ),
      
      debugShowCheckedModeBanner: false
    );
  }
}